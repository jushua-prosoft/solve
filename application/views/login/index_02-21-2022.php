<section>
  <div class="container-fluid"><!--container-fluid sub-header-->
        <div class="row"><!--row sub-header-->
          <div class="container" style="z-index:10;padding-left:30px;">
            <?php $this->load->view('layouts/header_nav'); ?>
          </div>
          <div class="container">
            <div class="col-md-7 " style="margin-top:30px;">
              <div class="login-container well">
					     <?php if($this->input->get('redirect_url')): ?>
						  <div class="alert alert-danger" role="alert">Please log-in to register.</div>
							<input type="hidden" id="get" value="<?=$this->input->get('redirect_url')?>">
		
						<?php endif; ?>
					
              <?php /*
                <form role="form">
                <div class="form-group">
                <input type="email" class="form-control login-email" id="" placeholder="Username or Email">
                </div>
                <div class="form-group">
                <input type="password" class="form-control login-password" id="" placeholder="Password">
                </div>
                <div class="checkbox">
                <label>
                <input type="checkbox"> Remember me
                </label>
                </div>
                <div class="checkbox">
                <label>
                <input type="checkbox"> I have read and agree to the <span class="hl-gray-6"><u>InVisage Non Disclosure Agreement</u></span>
                </label>
                </div>
                <div class="center" style="float:right;margin-top:20px !important;">
                <button type="submit" class="btn btn-login-submit text-right"><span class="hl-white">Log In</span></button>
                <p class="hl-gray-6 center" style="	line-height:2em;"><u>Lost your password?</u></p>
                </div>
                </form>
               */ ?>
                        <script src="<?php echo base_url() ?>js/login.js"></script>
                                <div class="login-section login-box">
                                    <form role="form" class="disable-enter-key">
									
                                        <div class="form-group" style="margin-bottom: 7px;">
                                            <input type="email" class="form-control" id="login-username" name="Email" placeholder="Email">
                                        </div>
                                        <div class="form-group" style="margin-bottom:0px;">
                                            <input type="password" class="form-control" id="login-password" name="Password" placeholder="Password">
                                        </div>
                                        <div class="checkbox"  style="margin-top:5px;">
                                            <label>
                                                <input type="checkbox" name="RememberMe" id="login-remember-me" class="remember-me"> <small>Remember me</small>
                                            </label>
                                        </div>
                                        <!--<div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="document-agreement"> 
                                                <small>
                                                    I have read and agree to
                                                    <a href="#non-disclosure-agreement" class="hl-gray" style="text-decoration: underline" data-toggle="modal" data-target="#non-disclosure-agreement">
                                                        InVisage Non Disclosure Agreement
                                                    </a>
                                                </small>
                                            </label>
                                        </div>
										-->
										
										
										<span style="display:table-cell;vertical-align:top;color:#ff192b;width:2%;height:40px;">*</span>
										<span style="display:table-cell;width:90%;">
											<small>
												By clicking the Login button below, you acknowledge you have read and agree to the
												<a href="#non-disclosure-agreement" class="hl-gray" style="text-decoration: underline" data-toggle="modal" data-target="#non-disclosure-agreement">
													SolveMasters Non Disclosure Agreement
												</a>
											</small>
										</span>
										
                                        <button type="submit" 
                                                class="btn btn-login center-block btn-login-submit" style="margin-top: 15px;" id="btn-login">
                                            Log In
                                        </button>
                                        <p class="text-center reset-margin">
                                            <a href="#" class="hl-gray btn-forgot-password" style="padding-top:10px !important; text-decoration: underline">
                                                <small>
                                                    Lost your password?
                                                </small>
                                            </a>
                                        </p>
                                    </form>
                                </div>

                                <div class="login-section forgot-password-box" style="display:none;">
                                    <form role="form">
                                        <br/>
                                        <br/>
                                        <div class="form-group" style="margin-bottom: 7px;">
                                            <input type="email" class="form-control" id="login-username" name="Email" placeholder="Email">
                                        </div>

                                        <button type="submit" 
                                                class="btn btn-forgot-password center-block btn-login-submit" id="btn-login">
                                            Send Email
                                        </button>
                                        <p class="text-center reset-margin">
                                            <a href="#" class="hl-gray btn-login" style="padding-top:10px !important; text-decoration: underline">
                                                <small>
                                                    Go back to Log-in?
                                                </small>
                                            </a>
                                        </p>
                                    </form>
                                </div>

                            <script type="text/javascript">
                                $(function () {
                                    $("#close-login").click(function () {
                                        $(".member-login").popover('hide');
                                    });
                                });
                            </script>
                        </div>
                    </div>
                    <?php /*
                    <div class="menu col-md-4  hidden-xs hidden-sm">
                        <ul>
                            <!--h1>Member Login</h1-->
                            <?php $this->load->view('layouts/sidebar_front'); ?>
                        </ul>
                    </div>
                    */ ?>
                </div>
            </div> <!--/row-->
			
			
		<!-- Modal -->
		<div class="modal fade" id="non-disclosure-agreement" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">	SolveMasters Nondisclosure Agreement</h4>
					</div>
					<div class="modal-body">
						<p>This Agreement (“Agreement”) is made and entered into by and between SOLVEMASTERS, LLC, a Michigan limited liability company (“Company”), and the afore signed prospective (“Client”).</p>
						<p>Company and Client contemplate that each of them may disclose certain information to the other of them (the Party who discloses such information is referred to as the “Discloser”; the Party who receives such information is the “Receiver”). The Parties desire to continue discussions while protecting their interests and intellectual properties. Discloser is willing to provide Receiver with, certain “Confidential Information”, but only if Receiver treats the same as confidential and proprietary to Discloser under the terms of this Agreement. In consideration of the foregoing, and the covenants and agreements set forth herein, the Parties agree as provided in this Agreement.</p>
						<ol> 
							<li style="margin:10px 0;">“Confidential Information” means Discloser’s proprietary or confidential data, documents, records and other information, including those relating to its business, plans, strategies, patients, referral sources, suppliers, employees, opportunities, financial statements and information, including tax returns, cost and pricing information, and related matters, and trade secrets, and any such information of third parties which Discloser has agreed, agrees or is required to maintain as confidential, and the existence and content of this Agreement. Confidential Information does not include information which (i) is contained in analyses, compilations, studies or other documents prepared by Company, or any of Company’s affiliates, partners, directors, officers, employees, representatives, agents, financing sources or advisors (collectively, its “Agents”), which contain or otherwise reflect such Confidential Information, either in its entirety or in summary form, provided it does not identify names of Discloser or its Agents or patients, (ii) is or becomes generally available to the public or the industry in which Discloser is engaged in business, other than as a result of disclosure in violation of this Agreement, (iii) is known to the Receiver prior to the execution thereof, or (iv) is made available to Receiver from a source other than Discloser or its advisors or representatives, and not, to Receiver’s knowledge, in violation of Discloser’s proprietary rights.</li>
							<li style="margin:10px 0;">Receiver and its Agents shall at all times hold the Confidential Information in confidence, and none of them will, without the prior written consent of Discloser, disclose or divulge any of the same in any manner whatsoever, in whole or in part to anyone, including any employee of Discloser. However, Receiver may disclose or divulge Confidential Information to those of its Agents who, in Receiver’s reasonable judgment, need to know the Confidential Information, provided that each such Agent agrees to be bound by the terms of this Agreement as a Receiver and does not breach or default under any obligation of Receiver in this Agreement. All of the Confidential Information which is provided directly or indirectly by Discloser is, will be and shall always remain the sole and exclusive property of Discloser.</li>
							<li style="margin:10px 0;">Upon Discloser’s written request, (i) Receiver will cause all Confidential Information and all copies thereof to be promptly returned to Discloser or destroyed, and any analyses, compilations, studies or other documents prepared by or for Receiver or its Agents from the Confidential Information to be destroyed, (ii) Receiver shall promptly confirm any such destruction in writing to Discloser, and (iii) unless permitted by Discloser in writing, neither Receiver nor any person or entity receiving any of the same from Receiver shall retain any copies thereof.</li>
							<li style="margin:10px 0;">No failure or delay by either Party in exercising any right, power or privilege under this Agreement shall operate as a waiver thereof, nor shall any single or partial exercise thereof preclude any other or further exercise of any right, power or privilege under this Agreement. This Agreement may not be amended or modified, or assigned by Receiver, except by written instrument signed by both Parties. This Agreement shall inure to the benefit of the Parties and Discloser’s successors and assigns. No licenses or rights under any patent, trademark, copyright or trade secret are granted or implied by this Agreement.</li>
							<li style="margin:10px 0;">If Receiver is required by law, regulation or court order to disclose any Confidential Information, Receiver will promptly notify Discloser in writing prior to making any such disclosure in order to permit Discloser to seek a protective order or other appropriate relief from the proper authority. Receiver shall cooperate with Discloser’s efforts to seek such order or other relief. If Discloser is not successful in precluding the requested legal body from requiring the disclosure of Confidential Information, Receiver will disclose only that portion of the Confidential Information which is legally required, and will exercise all reasonable efforts to assure that confidential treatment will be afforded the Confidential Information.</li>
							<li style="margin:10px 0;">Receiver recognizes that irreparable injury may result to Discloser and its business and property if Receiver breaches or defaults upon any of the provisions of this Agreement, and that money damages may not be a sufficient or adequate remedy for any such breach or default. Therefore, if Receiver or its Agents should engage in any act in violation or threatened violation of any of the provisions of this Agreement, Discloser shall be entitled to seek, in addition to other remedies, damages, expenses and relief available under applicable law, a temporary restraining order, preliminary injunction and/or permanent injunction, all without bond, prohibiting Receiver and its Agents from engaging in any such act, or specifically enforcing this Agreement, as the case may be. Receiver shall not assert any claim or defense that Discloser is not being irreparably harmed, or that money damages are a sufficient remedy, or that a bond shall be required. The prevailing party in any such proceedings shall be awarded reasonable attorneys’ fees against the non-prevailing party.</li>
							<li style="margin:10px 0;">This Agreement shall be governed by and construed in accordance with the laws of the state of Michigan without giving effect to the principles of conflicts of laws.</li>
						</ol>
					</div>
					<div class="modal-footer">
						<button type="button" style="background-color:#898989;color:#fff;" class="btn accept" data-dismiss="modal">Accept</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
			
  </div> <!--/container-->
</section> 