<!-- /.NAV FOR MOBILE ONLY -->

<header class="header">
   <div class="row reset-margin">
        <div class="container">
            <div class="col-sm-12">
                <!-- logo -->
                <h1 class="logo-container"><a href="<?php echo base_url(); ?>" id="logo"><img src="<?= base_url("img/solve-masters-logo-1.png"); ?>" id="logo" class="img-responsive" title="Solve Masters" alt="SolveMasters"></a></h1>
            </div>
        </div>  
    </div>
</header>